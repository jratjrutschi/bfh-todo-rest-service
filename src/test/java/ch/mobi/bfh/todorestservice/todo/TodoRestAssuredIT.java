package ch.mobi.bfh.todorestservice.todo;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TodoRestAssuredIT {

	@LocalServerPort
	int port;
	@Value("${auth.token}")
	private String authToken;

	@Before
	public void init() {
		RestAssured.port = port;
		RestAssured.basePath = "/todos";
	}

	@Test
	public void addTodo() {
		Todo todo = new Todo("Test", new Date());
		given().contentType(ContentType.JSON).body(todo)
				.when().post().then().statusCode(200).body("id", not(nullValue())).body("title", is("Test"));
	}

	@Test
	public void getTodos() {
		given().when().get().then().statusCode(200).body("size()", greaterThan(0));
	}

	@Test
	public void getTodo() {
		given().when().get("/" + 1).then().statusCode(200).body("id", is(1));
	}

	@Test
	public void completeTodo() {
		Todo todo = new Todo("Test", new Date());
		todo.setCompleted(true);
		given().contentType(ContentType.JSON).body(todo)
				.when().put("/" + 1).then().statusCode(200).body("completed", is(true));
	}

	@Test
	public void deleteTodo() {
		given().delete("/" + 2);
		given().when().get("/" + 2).then().statusCode(404);
	}

	private RequestSpecification given() {
		return RestAssured.given().header("AuthToken", authToken);
	}
}
