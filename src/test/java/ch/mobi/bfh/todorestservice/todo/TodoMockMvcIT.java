package ch.mobi.bfh.todorestservice.todo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TodoMockMvcIT {

	private static final String basePath = "/todos";

	@Autowired
	private MockMvc mockMvc;
	@Value("${auth.token}")
	private String authToken;

	@Test
	public void addTodo() throws Exception {
		Todo todo = new Todo("Test", new Date());
		mockMvc.perform(post(basePath).header("AuthToken", authToken)
				.contentType(MediaType.APPLICATION_JSON).content(asJson(todo)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("id").value(not(nullValue())))
				.andExpect(jsonPath("title").value("Test"));
	}

	@Test
	public void getTodos() throws Exception {
		mockMvc.perform(get(basePath).header("AuthToken", authToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", not(empty())));
	}

	@Test
	public void getTodo() throws Exception {
		mockMvc.perform(get(basePath + "/" + 1).header("AuthToken", authToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("id").value(1));
	}

	@Test
	public void completeTodo() throws Exception {
		Todo todo = new Todo("Test", new Date());
		todo.setCompleted(true);
		mockMvc.perform(put(basePath + "/" + 1).header("AuthToken", authToken)
				.contentType(MediaType.APPLICATION_JSON).content(asJson(todo)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("completed").value(true));
	}

	@Test
	public void deleteTodo() throws Exception {
		mockMvc.perform(delete(basePath + "/" + 2).header("AuthToken", authToken)).andExpect(status().isOk());
		mockMvc.perform(get(basePath + "/" + 2).header("AuthToken", authToken)).andExpect(status().isNotFound());
	}

	private String asJson(Object object) throws Exception {
		return new ObjectMapper().writeValueAsString(object);
	}
}
