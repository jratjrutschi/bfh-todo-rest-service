package ch.mobi.bfh.todorestservice.todo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TodoRestTemplateIT {

	private static final String basePath = "/todos";

	@Autowired
	private TestRestTemplate restTemplate;
	@Value("${auth.token}")
	private String authToken;

	@Before
	public void init() {
		restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(
				(request, body, execution) -> {
					request.getHeaders().add("AuthToken", authToken);
					return execution.execute(request, body);
				}
		));
	}

	@Test
	public void addTodo() {
		Todo todo = new Todo("Test", new Date());
		todo = restTemplate.postForObject(basePath, todo, Todo.class);
		assertThat(todo.getId(), not(nullValue()));
		assertThat(todo.getTitle(), is("Test"));
	}

	@Test
	public void getTodos() {
		List<Todo> todos = restTemplate.getForObject(basePath, List.class);
		assertThat(todos, not(empty()));
	}

	@Test
	public void getTodo() {
		Todo todo = restTemplate.getForObject(basePath + "/" + 1, Todo.class);
		assertThat(todo.getId(), is(1L));
	}

	@Test
	public void completeTodo() {
		Todo todo = new Todo("Test", new Date());
		todo.setCompleted(true);
		todo = restTemplate.exchange(basePath + "/" + 1, HttpMethod.PUT, new HttpEntity(todo), Todo.class).getBody();
		assertThat(todo.isCompleted(), is(true));
	}

	@Test(expected = RestClientException.class)
	public void deleteTodo() {
		restTemplate.delete(basePath + "/" + 2);
		restTemplate.getForObject(basePath + "/" + 2, Todo.class);
	}
}
