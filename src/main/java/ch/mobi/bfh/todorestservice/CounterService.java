package ch.mobi.bfh.todorestservice;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
@Scope(value = "singleton")
public class CounterService {

    private final AtomicInteger amountRequests;

    public CounterService() {
        amountRequests = new AtomicInteger(0);
    }

    public AtomicInteger getAmountRequests() {
        return amountRequests;
    }

    public void addAmountRequests() {
        this.amountRequests.incrementAndGet();
    }
}
