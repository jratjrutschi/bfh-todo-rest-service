package ch.mobi.bfh.todorestservice;

import ch.mobi.bfh.todorestservice.tasks.RequestLoggerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

@SpringBootApplication
@EnableScheduling
public class Application implements CommandLineRunner {

	@Autowired
	private ThreadPoolTaskScheduler taskScheduler;

	@Autowired
	private RequestLoggerTask requestLoggerTask;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*
			Aktuell: jede Minute zur 0. Sekunde!
			Beispiel: "0/10 * * * * ?" um alle 10 Sekunden zu schedulen!
		 */
		CronTrigger cronTrigger = new CronTrigger("0 * * * * ?");
		taskScheduler.schedule(requestLoggerTask, cronTrigger);
	}
}

