package ch.mobi.bfh.todorestservice.todo;

public class TodoNotFoundException extends Exception {

	public TodoNotFoundException(String message) {
		super(message);
	}
}
