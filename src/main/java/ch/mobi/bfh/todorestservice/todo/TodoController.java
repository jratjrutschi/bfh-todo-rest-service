package ch.mobi.bfh.todorestservice.todo;

import ch.mobi.bfh.todorestservice.CounterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/todos")
@Api(value = "TodoService", description = "description vor /todo-Mapping")
public class TodoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TodoController.class);

	@Autowired
	private TodoRepository todoRepository;

	@Autowired
	private CounterService counterService;

	@PostMapping(
			consumes = "application/json",
			produces = "application/json"
	)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "place Todo",  httpMethod = "POST", notes = "Will return the saved Todo")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "order places registered"),
			@ApiResponse(code = 400, message = "invalid order data"),
			@ApiResponse(code = 404, message = "book or customer not found"),
			@ApiResponse(code = 406, message = "unsupported accept type"),
			@ApiResponse(code = 415, message = "unsupported content type"),
			@ApiResponse(code = 422, message = "payment failed")
	})
	public Todo addTodo(@RequestBody @Valid Todo todo) {
		counterService.addAmountRequests();

		todo = todoRepository.save(todo);
		LOGGER.info("Todo with id " + todo.getId() + " added");
		return todo;
	}

	@GetMapping
	public List<Todo> getTodos() {
		counterService.addAmountRequests();

		return todoRepository.findAll();
	}

	@GetMapping("{id}")
	public Todo getTodo(@PathVariable long id) throws TodoNotFoundException {
		counterService.addAmountRequests();

		return todoRepository.findById(id)
				.orElseThrow(() -> new TodoNotFoundException("Todo with id " + id + " not found"));
	}

	@PutMapping("{id}")
	public Todo updateTodo(@PathVariable long id, @RequestBody @Valid Todo todo) throws TodoNotFoundException {
		counterService.addAmountRequests();

		if (todo.getId() == null) todo.setId(id);
		if (todo.getId() != id)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Todo has an invalid id");
		getTodo(todo.getId());
		todo = todoRepository.save(todo);
		LOGGER.info("Todo with id " + todo.getId() + " updated");
		return todo;
	}

	@DeleteMapping("{id}")
	public void removeTodo(@PathVariable long id) {
		counterService.addAmountRequests();

		todoRepository.findById(id).ifPresent(todo -> todoRepository.delete(todo));
		LOGGER.info("Todo with id " + id + " deleted");
	}

	@ExceptionHandler
	public ResponseEntity<String> handle(TodoNotFoundException ex) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
	}
}
