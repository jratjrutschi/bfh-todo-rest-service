package ch.mobi.bfh.todorestservice.todo;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

public class TodoCSVMessageConverter implements HttpMessageConverter<List<Todo>> {

	private static final MediaType MEDIA_TYPE = new MediaType("text", "csv");

	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		return false;
	}

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return List.class.isAssignableFrom(clazz) && (mediaType == null || mediaType.equals(MEDIA_TYPE));
	}

	@Override
	public List<MediaType> getSupportedMediaTypes() {
		return Collections.singletonList(MEDIA_TYPE);
	}

	@Override
	public List<Todo> read(Class<? extends List<Todo>> clazz, HttpInputMessage message)
			throws IOException, HttpMessageNotReadableException {
		return null;
	}

	@Override
	public void write(List<Todo> todos, MediaType contentType, HttpOutputMessage message)
			throws IOException, HttpMessageNotWritableException {
		try (PrintWriter writer = new PrintWriter(message.getBody())) {
			todos.forEach(todo -> {
				writer.println(todo.getId() + "," + todo.getTitle() + "," + todo.getDueDate() + "," + todo.isCompleted());
			});
		}
	}
}
