package ch.mobi.bfh.todorestservice.todo;

import ch.mobi.bfh.todorestservice.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/count")
public class CounterController {

    @Autowired
    private CounterService counterService;

    @GetMapping
    public ResponseEntity<String> getCountRequests() {
        int countRequests = counterService.getAmountRequests().get();
        return new ResponseEntity<>(
                "Anzahl Requests: " + countRequests,
                HttpStatus.OK);
    }

}
