package ch.mobi.bfh.todorestservice;

import org.springframework.beans.factory.annotation.Value;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

public class AuthFilter implements Filter {

	@Value("${auth.token}")
	private String authToken;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String header = ((HttpServletRequest) request).getHeader("AuthToken");
		if (header == null)
			((HttpServletResponse) response).sendError(SC_UNAUTHORIZED, "Missing authentication token");
		else if (!header.equals(authToken))
			((HttpServletResponse) response).sendError(SC_UNAUTHORIZED, "Invalid authentication token");
		else chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}
}
