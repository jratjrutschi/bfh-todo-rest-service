package ch.mobi.bfh.todorestservice.tasks;

import ch.mobi.bfh.todorestservice.CounterService;
import ch.mobi.bfh.todorestservice.todo.TodoController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequestLoggerTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TodoController.class);

    @Autowired
    private CounterService counterService;

    public RequestLoggerTask() {
    }

    public void run() {
        LOGGER.info(counterService.getAmountRequests().toString());
    }
}